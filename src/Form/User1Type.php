<?php

namespace App\Form;

use App\Entity\User;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\BirthdayType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\RepeatedType;

class User1Type extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('email')
            ->add('name')
            ->add('firstname')
            ->add('currentLocation')
            ->add('address')
            ->add('gender', ChoiceType::class, array(
                'choices' => array(
                    'Male' => 'male',
                    'Female' => 'female',
                    'Transgender' => 'transgender',
                )
            ))
            ->add('country')
            ->add('nationality')
            ->add('passport')
            ->add('cv', FileType::class, array(
                'data_class' => null,
                ))
            ->add('picture')
            ->add('birthdate', BirthdayType::class, array(
                'placeholder' => array(
                    'day' => 'Day', 'month' => 'Month', 'year' => 'Year', 
                )
            ))
            ->add('jobSector', ChoiceType::class, array(
                'choices' => array(
                    'Commercial' => 'Commercial',
                    'Retail sales' => 'Retail sales',
                    'Creative' => 'Creative',
                    'Technology' => 'Technology',
                    'Marketing & PR' => 'Marketing & PR',
                    'Fashion & luxury' => 'Fashion & luxury',
                    'Management & HR' => 'Management & HR',
                )
            ))
            ->add('experience', ChoiceType::class, array(
                'choices' => array(
                    'O - 6 month' => '0-6 month',
                    '6 month - 1 year' => '6 month - 1 year',
                    '1 - 2 years' => '1 - 2 years',
                    '2+ years' => '2+ years',
                    '5+ years' => '5+ years',
                    '10+ years' => '10+ years',
                )
            ))
            ->add('shortDescription')
            ->add('birthplace')
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => User::class,
        ]);
    }
}
