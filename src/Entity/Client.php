<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\ClientRepository")
 */
class Client
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $societyName;

    /**
     * @ORM\Column(type="string", length=155, nullable=true)
     */
    private $contactName;

    /**
     * @ORM\Column(type="string", length=155)
     */
    private $contactEmail;

    /**
     * @ORM\Column(type="string", length=50, nullable=true)
     */
    private $contactPhone;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $societyType;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $contactPost;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $notes;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getSocietyName(): ?string
    {
        return $this->societyName;
    }

    public function setSocietyName(string $societyName): self
    {
        $this->societyName = $societyName;

        return $this;
    }

    public function getContactName(): ?string
    {
        return $this->contactName;
    }

    public function setContactName(?string $contactName): self
    {
        $this->contactName = $contactName;

        return $this;
    }

    public function getContactEmail(): ?string
    {
        return $this->contactEmail;
    }

    public function setContactEmail(string $contactEmail): self
    {
        $this->contactEmail = $contactEmail;

        return $this;
    }

    public function getContactPhone(): ?string
    {
        return $this->contactPhone;
    }

    public function setContactPhone(?string $contactPhone): self
    {
        $this->contactPhone = $contactPhone;

        return $this;
    }

    public function getSocietyType(): ?string
    {
        return $this->societyType;
    }

    public function setSocietyType(?string $societyType): self
    {
        $this->societyType = $societyType;

        return $this;
    }

    public function getContactPost(): ?string
    {
        return $this->contactPost;
    }

    public function setContactPost(?string $contactPost): self
    {
        $this->contactPost = $contactPost;

        return $this;
    }

    public function getNotes(): ?string
    {
        return $this->notes;
    }

    public function setNotes(?string $notes): self
    {
        $this->notes = $notes;

        return $this;
    }
}
