<?php

namespace App\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use App\Repository\JobOfferRepository;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class HomeController extends AbstractController
{
    /**
     * @Route("/", name="home")
     */
    public function index(JobOfferRepository $jobOfferRepository)
    {
        return $this->render('home/index.html.twig', [
            'controller_name' => 'HomeController',
            'job_offers' => $jobOfferRepository->findAll()
        ]);
    }

    /**
     * @Route("/about", name="about")
     */
    public function aboutUs()
    {
        return $this->render('about.html.twig');
    }

    /**
     * @Route("/contact", name="contact")
     */

    public function contact()
    {
        return $this->render('contact.html.twig');
    }
}
