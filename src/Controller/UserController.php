<?php

namespace App\Controller;

use App\Entity\User;
use App\Form\User1Type;
use App\Service\FileUploader;   
use App\Repository\UserRepository;
use Symfony\Component\HttpFoundation\Request;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;


class UserController extends AbstractController
{
    // /**
    //  * @Route("/", name="user_index", methods="GET")
    //  */
    // public function index(UserRepository $userRepository): Response
    // {
    //     return $this->render('user/index.html.twig', ['users' => $userRepository->findAll()]);
    // }

    // /**
    //  * @Route("/new", name="user_new", methods="GET|POST")
    //  */
    // public function new(Request $request): Response
    // {
    //     $user = new User();
    //     $form = $this->createForm(User1Type::class, $user);
    //     $form->handleRequest($request);

    //     if ($form->isSubmitted() && $form->isValid()) {
    //         $em = $this->getDoctrine()->getManager();
    //         $em->persist($user);
    //         $em->flush();

    //         return $this->redirectToRoute('user_index');
    //     }

    //     return $this->render('user/new.html.twig', [
    //         'user' => $user,
    //         'form' => $form->createView(),
    //     ]);
    // }

    // /**
    //  * @Route("/{id}", name="user_show", methods="GET")
    //  */
    // public function show(User $user): Response
    // {
    //     return $this->render('user/show.html.twig', ['user' => $user]);
    // }

    /**
     * @Route("/profile", name="user_edit", methods="GET|POST")
     */
    public function edit(Request $request, ObjectManager $em, FileUploader $fileUploader)
    {
        $user = $this->getUser();
        
        $this->denyAccessUnLessGranted('IS_AUTHENTICATED_FULLY');

        $form = $this->createForm(User1Type::class, $user);
        
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {

        $file = $user->getCv();
        $fileName = $fileUploader->upload($file);

        $user->setCv($fileName);

        $em->persist($user);
        $em->flush();

            return $this->redirectToRoute('user_edit');
        }

        return $this->render('user/edit.html.twig', [
            'user' => $user,
            'form' => $form->createView(),
        ]);
    }

    // /**
    //  * @Route("/{id}", name="user_delete", methods="DELETE")
    //  */
    // public function delete(Request $request, User $user): Response
    // {
    //     if ($this->isCsrfTokenValid('delete'.$user->getId(), $request->request->get('_token'))) {
    //         $em = $this->getDoctrine()->getManager();
    //         $em->remove($user);
    //         $em->flush();
    //     }

    //     return $this->redirectToRoute('user_index');
    // }
}
